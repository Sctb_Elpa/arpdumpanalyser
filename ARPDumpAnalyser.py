#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys 
import json
import getopt
from DataProcessor import DataProcessor
from print_err import print_err

def createDataProcessorInstance(fileName, outdir='', noCoeffs=False):
	coeffsFileName=''
	lastDotPos = 0
	if not noCoeffs:
		while True:
			t = fileName.find('.', lastDotPos + 1)
			if t == -1:
				break
			else:
				lastDotPos = t
		if lastDotPos == 0:
			coeffsFileName = fileName + '.coeffs'
		else:
			coeffsFileName = fileName[0:lastDotPos] + '.coeffs'
		
		try: 
			coeffsfile = open(coeffsFileName)
		except FileNotFoundError:
			print_err('Файл коэфициентов "%s" не найден' % coeffsFileName)
			return False
	
	try:
		dataFile = open(fileName, 'rb')	
	except FileNotFoundError:
		print_err('Файл дампа "%s" не найден' % fileName)
		return False
	
	if outdir == '':
		outdir = fileName + '.out/'
	else:
		if outdir[-1] != '/':
			outdir += '/'
	
	if not os.path.isdir(outdir):
		os.mkdir(outdir)

	try:
		if noCoeffs:
			d = DataProcessor(
			dataFile,
			outdir)
		else:
			d = DataProcessor(
			dataFile,
			outdir, 
			dict(json.load(coeffsfile)))
	except:
		print_err('Ошибка чтения файла коэфициентов')
		return False
	else:
		if not noCoeffs:
			coeffsfile.close()
	return d

def showUsage():
	print_err('''ARPDumpAnalyser.py [options] <dumpFile1> [dumpFile2] ...
Options:
 -o <Dir>\t Directory to write result files. (Default: as dumpFile name)
 --nocoeffs\t Don't use '.coeffs' files. (Default: Disabled)
 --saveheads\t Save block headers to file. (Default: Disabled)
 -h\t\t Show this message.
 
Normally, program have no output messages.
''')
				
def main():
	fNoCoeffs = False
	fSaveHeads = False
	savePath = ''
	try:
		optlist, args = getopt.getopt(sys.argv[1:], 'ho:', ['nocoeffs', 'saveheads'])
	except:
		showUsage()
		quit()
	else:
		if ('-h', '') in optlist:
			# показать справку и выйти
			showUsage()
			quit()
		if len(args) == 0:
			print_err('No input files!')
			showUsage()
			quit()
		if ('--nocoeffs', '') in optlist:
			fNoCoeffs = True
		if ('--saveheads', '') in optlist:
			fSaveHeads = True
		for opt in optlist:
			if opt[0] == '-o':
				if opt[1] != '':
					savePath = opt[1]
					break
				else:
					print_err('\'-o\' mast have argument!')
					quit()
							
		processors = []
		for inFile in args:
			t = createDataProcessorInstance(inFile, outdir=savePath, noCoeffs=fNoCoeffs)
			if t:
				processors.append(t)
				
		#обработка
		for processor in processors:
			processor.doWork(fSaveHeads)
			processor.Input.close()
			
			
	
# чтобы при импорте не выполнялся код автоматом
if __name__ == '__main__':
	main()
