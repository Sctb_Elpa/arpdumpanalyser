
# -*- coding: utf-8 -*-
from struct import unpack
import MyDateTime

class HeaderError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def BCDToByte(sourceByte):
    return ((sourceByte & 0xF0) >> 4) * 10 + (sourceByte & 0x0F)

class DataHeader:
    """
    Timestamp: 6 байт
    WriteInterval: 2 байта
    StartPressureCntrVal: 3 байта
    StartTemperatureCntrVal: 3 байта
    batteryVoltage: 4 байта
    '=' у описания структуры 
    Character     Byte order     Size     Alignment
    @     native     native     native
    =     native     standard     none
    """
    def __init__(self, headerData):
        if len(headerData) != 22:
            raise HeaderError('Неверная длина шапки (%i)' % len(headerData))
        _Head_, Timestamp, WriteInterval, Startval, self.batteryVoltage = unpack('=4s 6s H 6s f', headerData)
        if _Head_ != bytes([0xff, 0xff, 0xff, 0xff]):
            raise HeaderError('Ошибка признака шапки')
        if (Timestamp == bytes([0xff, 0xff, 0xff, 0xff, 0xff, 0xff])) or (
            Startval == bytes([0xff, 0xff, 0xff, 0xff, 0xff, 0xff])):
            raise HeaderError('Обнаружено повреждение полей шапки') 
        self.DateTime = MyDateTime.myDateTime(BCDToByte(Timestamp[4]) + 2000, BCDToByte(
            Timestamp[5]), BCDToByte(Timestamp[2]), BCDToByte(Timestamp[3]), BCDToByte(
            Timestamp[0]), BCDToByte(Timestamp[1])) #ok
        self.WriteInterval = int(WriteInterval * 1000 / 64)  
        self.StartPressureCntrVal = (Startval[1] << 16) + (Startval[0] << 8) + Startval[3]  #ok 
        self.StartTemperatureCntrVal = (Startval[2] << 16) + (Startval[5] << 8) + Startval[4] #ok
    def __str__(self):
        date = str(self.DateTime)
        if date[0] == '<':
            date = date[1:date.index('>')]
        return '''Дата: %s
Интервал записи (мс): %i
Нач. Уст. счетчика давления: %i
Нач. Уст. счетчика температуры: %i
напр. батареи(В): %f''' % (
                    date, self.WriteInterval, self.StartPressureCntrVal, 
                    self.StartTemperatureCntrVal, self.batteryVoltage)    

    
