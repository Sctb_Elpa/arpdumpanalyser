
# -*- coding: utf-8 -*-

from print_err import print_err
from DataHeader import DataHeader, HeaderError
from MyDateTime import myDateTime

csvDeterminator = ';'
    
F_CPU = 3686400

class DataBlockError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    
def CalculePreasureAndTemp(data, coeffs, StartValues):
    try:
        PreasureCounterValue = (0xffffff - StartValues[0]) / (
                (data[1] << 16) + (data[0] << 8) + data[3]) * (F_CPU / 4)
        TemperatureCounterValue = (0xffffff - StartValues[1]) / (
                (data[2] << 16) + (data[5] << 8) + data[4]) * (F_CPU / 4)
    except ZeroDivisionError:
        return False
    else:
        ftr = TemperatureCounterValue - coeffs['Ft0']
        fpr = PreasureCounterValue - coeffs['Fp0']
        osn = TemperatureCounterValue - coeffs['F0']
        P = coeffs['A0'] + coeffs['A1'] * ftr + (
            coeffs['A2'] * ftr ** 2) + coeffs['A3'] * fpr + (
            coeffs['A4'] * fpr ** 2) + coeffs['A5'] * ftr * fpr
        T = coeffs['T0'] + coeffs['C1'] * osn ** 1 + (
            coeffs['C2'] * osn ** 2) + coeffs['C3'] * osn ** 3
        #return (str(P).replace('.', ','), str(T).replace('.', ','))
        return ('{:.3f}'.format(P).replace('.', ','),'{:.3f}'.format(T).replace('.', ','))
    
def ParseDataFunConstructor(Header, coeffs):
    def Result(data, offset):
        PT = CalculePreasureAndTemp((
            data), coeffs, (Header.StartPressureCntrVal, Header.StartTemperatureCntrVal))
        if PT != False:
            return (Header.DateTime.addMilisecond(offset * Header.WriteInterval), ) + PT
        else:
            return False 
    return Result

class DataProcessor:
    """
    Процессор данных
    """
    def __init__(self, inData, outFileNameMeta='', coeffs={
                                        "A0": 0,
                                        "A1": 0,
                                        "A2": 0,
                                        "A3": 1,
                                        "A4": 0,
                                        "A5": 0,
                                        "Ft0": 0,
                                        "Fp0": 0,
                                        "T0": 0,
                                        "C1": 1,
                                        "C2": 0,
                                        "C3": 0,
                                        "F0": 0
                                    }):
        self.Input = inData
        self.OutFileNameMeta = outFileNameMeta
        self.Coeffs = coeffs
        self.heads = []
            
    def doWork(self, saveheads = False):
        LastDate = False
        FirstHeadFound = False
        data = b''
        startOffset = 0
        CurentHeader = False
        while True:            
            #ищем первую шапку
            try:
                while True:
                    t, data = self.readIfLess(data, 4)
                    if (not t) and (not FirstHeadFound):
                        raise HeaderError('Не найдено ни одной шапки')
                    headerPos = data.find(bytes([0xff, 0xff, 0xff, 0xff]))
                    if headerPos != -1:
                        # признак шапки найден
                        t, data = self.readIfLess(data, 22)
                        if not t:
                            raise HeaderError('Недостаточно данных на напку')# данные кончились
                        if not FirstHeadFound:
                            data = data[headerPos:] #отрезаем все до признака шапки
                            FirstHeadFound = True
                        try:
                            NewHeader = DataHeader(data[:22]) # пытаемся распарсить шапку
                            if NewHeader.DateTime.illegalDateString != '':
                                print_err('Illegal date found: %s' % NewHeader.DateTime.illegalDateString)
                        except HeaderError: # ошибка парсинга шапки
                            data = data[22:] # удалаем битую шапку, продолжаем поиск
                            continue
                        else:
                            data = data[22:] # удалаем шапку, для парсинга она не нужна
                            '''
                            Если предидущая дата существует и шапка нового пакета корректна, можно проверить
                            на назрывы между данными
                            '''
                            if LastDate and NewHeader.DateTime.illegalDateString == '' and CurentHeader:
                                diff = LastDate.addMilisecond(CurentHeader.WriteInterval) - NewHeader.DateTime
                                if diff > 0:
                                    # Лишние данные
                                    print_err('[WARNING] Extra data items (%i) found at block %i' % (
                                              diff / CurentHeader.WriteInterval,
                                              len(self.heads)))
                                if diff < 0:
                                    # мало данных
                                    print_err('[WARNING] Too few data items (%i) found at block %i' % (
                                            -diff / CurentHeader.WriteInterval,
                                            len(self.heads)))
                            CurentHeader = NewHeader
                            break # шапка найдена и успешно распознана  
                    elif FirstHeadFound:
                        break 
            except HeaderError:
                break # заканчиваем             
            t, data = self.readIfLess(data, 8192 * 2)
            if len(data) < 6:
                break #конец данных, выход
            headerPos = data.find(bytes([0xff, 0xff, 0xff, 0xff])) # поиск шапки во вновь прочитанном
            if headerPos != -1: 
                #Где-то вперди новая шапка
                MeasureData = data[:headerPos] # Это парсить
                DataLenC, DataLenO = divmod(len(MeasureData), 6) # проверка выравнивания
                MeasureData = MeasureData[:DataLenC * 6]
                data = data[headerPos:] # это на следующую итарацию, больше не трогаем её

                # ловушка глюковой ситуации:
                # Запись искажается таким образом, что идет шапка, потом 1 измерение, потом опять шапка.
                # Выведем варнинг и не будем обрабатывать такие данные
                if len(MeasureData) == 6:
                    print_err('[WARNING] Обнаружен единичный пакет {}, пропуск'.format(len(self.heads)))
                    data =  b''
                    continue
            else:
                DataLenC, DataLenO = divmod(len(data), 6) # проверка выравнивания
                MeasureData = data[:DataLenC * 6] # Это парсить
                if DataLenO == 0:
                    data = b''
                else:
                    data = data[-DataLenO:] # это на cледующий следующую итарацию, больше не трогаем её
                
            # теперь есть заголовок (CurrentHeader) и данные в (MeasureData), можно парсить!
            # шаг 1: data[] -> data[][6] - разбиене списка на спски по 6 байт
            if len(self.heads) == 0:
                openMode = 'w'
                startOffset = 0
            elif self.heads[-1] != CurentHeader:
                openMode = 'w'
                startOffset = 0
            else:
                openMode = 'a'
                    
            MeasureData = [MeasureData[i:i+6] for i in range(0, len(MeasureData), 6)]
            
            dataParser = ParseDataFunConstructor(CurentHeader, self.Coeffs)
            # шаг 2: data[][6] -> strings[] - пощитать с коэфициентами (map)
            startOffsetadd = len(MeasureData)
            MeasureData = map(dataParser, MeasureData, range(startOffset, len(MeasureData)))
            startOffset += startOffsetadd
            
            # Вывод
            if self.OutFileNameMeta == '':
                #stdo
                for measure in MeasureData:
                    print(csvDeterminator.join(map(str, measure)) + '\n', flush=True)
            else:
                #file
                dateStr = str(CurentHeader.DateTime)
                if CurentHeader.DateTime.clockStopDetected:
                    dateStr = '+' + dateStr
                if self.OutFileNameMeta[-1] == '/':
                    outFileName = self.OutFileNameMeta + dateStr.replace(' ', '_') + '.csv'
                else:
                    outFileName = self.OutFileNameMeta + '.' + dateStr.replace(' ', '_') + '.csv'
                if outFileName[1] == ':': # windows path like C:/blablabla
                    outFileName = outFileName[0:2] + outFileName[2:].replace(':', '-')
                else:
                    outFileName = outFileName.replace(':', '-')
                if '>' in outFileName:
                    outFileName = outFileName[:outFileName.find('>')] + '#.csv'   
                    outFileName = outFileName.replace('<', '#')
                try:
                    outfile = open(outFileName, openMode)
                    if openMode == 'w':
                        outfile.write(csvDeterminator.join(('Дата', 'Значение давления', 'Значение температуры')) + '\n') # заголовок таблицы
                    for measure in MeasureData:
                        if measure:
                           outfile.write(csvDeterminator.join(map(str, measure)) + '\n')
                    outfile.close()
                except:
                    print_err('Ошибка записи в файл: %s' % outFileName)
                    
                try:
                    LastDate = measure[0]
                    print('Обработан: блок {}'.format(LastDate), flush=True)
                except:
                    LastDate = False
            
            self.heads.append(CurentHeader) # сохранение шапки
            
        if saveheads:
            if self.OutFileNameMeta[-1] == '/':
                headFileName = self.OutFileNameMeta + 'heads.txt'
            else:
                headFileName = self.OutFileNameMeta + '.heads.txt'
            try:
                headfile = open(headFileName, 'w')
                headfile.write('=' * 20 + '\n')
                headfile.write('\n'.join(map(lambda header: str(header) + '\n' + '=' * 20, self.heads)))
                headfile.close()
            except:
                print_err('Ошибка записи данных заголовка %s' % headFileName)

    def readIfLess(self, f, nessusry):
        if len(f) < nessusry:
            f += self.Input.read(8192 * 2)
        return ((len(f) >= nessusry), f)
                
                    
        