
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

class myDateTime:
    def __init__(self, year=0, month=0, day=0, hour=0, minute=0, second=0, milisecond=0):
        self.illegalDateString = ''
        # Если резонатор в частах останавливался, то в старшем бите секунд будет 1
        # но уже сделано BCD преобразование, значи эта единица дает +80 к значению секунд, вычитаем его
        self.clockStopDetected = False
        if second > 80:
            second = second - 80
            self.clockStopDetected = True
        try:
            self.normalDatatime = datetime(year, month, day, hour, minute, second, milisecond * 1000)
        except:
            self.normalDatatime = datetime.now()
            self.illegalDateString = '%d.%02d.%02d %02d:%02d:%02d.%03d' % (
                year, month, day, hour, minute, second, milisecond)
            self.TimeDelta = timedelta(microseconds=0);
    def __str__(self):
        if len(self.illegalDateString) != 0:
            return '<%s> + %02d:%02d.%03d' % (
                self.illegalDateString, self.TimeDelta.seconds / 60, self.TimeDelta.seconds % 60, 
                self.TimeDelta.microseconds / 1000)
        else:
            return '%d.%02d.%02d %02d:%02d:%02d.%03d' % (
                self.normalDatatime.year, self.normalDatatime.month, self.normalDatatime.day,
                self.normalDatatime.hour, self.normalDatatime.minute, self.normalDatatime.second, 
                self.normalDatatime.microsecond / 1000)
    def addMilisecond(self, milisecond):
        result = myDateTime()
        if len(self.illegalDateString) != 0:
            result.illegalDateString = self.illegalDateString
            result.TimeDelta = self.TimeDelta + timedelta(microseconds=milisecond*1000)
        else:
            result.illegalDateString = ''
            result.normalDatatime = self.normalDatatime + timedelta(microseconds=milisecond*1000)
        return result 

    def __lt__(self, other): # self < other
        if isinstance(other, myDateTime):
            if self.illegalDateString != '' or other.illegalDateString != '':
                return False # невозможно сравнить
            else:
                return self.normalDatatime < other.normalDatatime
        else:
            return NotImplemented
        
    def __gt__(self, other): # self > other
        if isinstance(other, myDateTime):
            if self.illegalDateString != '' or other.illegalDateString != '':
                return False # невозможно сравнить
            else:
                return self.normalDatatime > other.normalDatatime
        else:
            return NotImplemented
        
    def __sub__(self, other):
        """
        Вычесть, результат в милисекундах
        """
        if self.illegalDateString != '' or other.illegalDateString != '':
            return 0 # невозможно вычесть
        else:
            delta = self.normalDatatime - other.normalDatatime
            return ((delta.days * 86400 + delta.seconds)*10**6 +
                delta.microseconds) / 10**3
