# -*- coding: utf-8 -*-

import sys


def print_err(*args):
    print(' '.join(map(str, args)), file=sys.stderr, flush=True)
    # sys.stderr.write(' '.join(map(str,args)) + '\n')
